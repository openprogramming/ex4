#include "finder.h"
#include "duplicatebuilder.h"

int main(int argc, char *argv[])
{
    if(argc != 2)
    {
        printf("Usage <name>.exe directory_to_check");
        return 1;
    }

    DuplicateBuilder db;
    Finder f( [&](const std::string& path, const std::string& name, int type)
    {
        if(type == dir)
        {
            //printf("Entity: %s\n", path.c_str());
        }
        else
            db.check(path, name);
    });

    f.Find(argv[1]);
    db.all();

    return 0;
}
