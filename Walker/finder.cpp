#include "finder.h"
#include <sstream>
#if _MSC_VER && !__INTEL_COMPILER
#   define _X86_
#   include <intrin.h>
#   include "include/dirent.h"
#else
#   include <dirent.h>
#   include <stdio.h>
#   include <string.h>
#endif

#ifdef WIN32
#   define DELIMITER "\\"
#else
#   define DELIMITER "/"
#endif

Finder::Finder(Function f)
{
    function = f;
}

void Finder::Find(const std::string& path)
{
    DIR* dir;
    struct dirent* entity;
    if((dir = opendir(path.c_str())) != NULL)
    {
        while((entity = readdir(dir)) != NULL)
        {
            switch (entity->d_type) {
            case DT_DIR:
            {
                if( !strcmp(entity->d_name, ".") || !strcmp(entity->d_name, "..") )
                    continue;



                std::stringstream newDir;
                newDir << path << DELIMITER << entity->d_name;
                function(newDir.str(), entity->d_name, EntryType::dir);
                Find(newDir.str());
            }
            break;
            default:
                std::stringstream newDir;
                newDir << path << DELIMITER << entity->d_name;
                function(newDir.str(), entity->d_name, file);
                break;
            }
        }
    }
}
