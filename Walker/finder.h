#ifndef FINDER_H
#define FINDER_H

#include <string>
#include <functional>

enum EntryType
{
    dir,
    file,
};

class Finder
{
public:
    typedef std::function<void (const std::string&, const std::string&, int)> Function;

    Finder( Function f );
    void Find(const std::string& path );

private:

    Function function;

};

#endif // FINDER_H
