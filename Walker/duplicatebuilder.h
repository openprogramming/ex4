#ifndef DUPLICATEBUILDER_H
#define DUPLICATEBUILDER_H

#include <map>
#include <set>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>

class DuplicateBuilder
{
public:
    struct Duplicate
    {
        std::string name;
        std::string path;
    };

protected:
    std::map< std::string, std::vector<Duplicate> > m_Duplicates;

public:
    DuplicateBuilder();
    virtual ~DuplicateBuilder(){}

    void check(const std::string& path, const std::string& file)
    {
        if(file.size() > 0)
        {
            Duplicate d;
            d.name = file;
            d.path = path;

            if(m_Duplicates.find(file) == m_Duplicates.end())
                m_Duplicates[file] = std::vector<Duplicate>();

            m_Duplicates[file].push_back(d);
        }
    }

    void all()
    {
        std::stringstream ss;
        std::for_each(m_Duplicates.begin(), m_Duplicates.end(), [&](const std::pair<std::string, std::vector<Duplicate>>& item)
        {
            if(item.second.size() == 1)
                return;

            ss << "Duplicate entry: " << item.first << " [x" << item.second.size() << "]";
            for(int i = 0; i < item.second.size(); ++i)
            {
                ss << "\nPath: " << item.second[i].path;
            }

            ss << "\n\n";
            //printf("%s\n", item.first.c_str());
        });

        printf(ss.str().c_str());

        std::ofstream generated("log.txt");
        generated << ss.str();

    }
};

#endif // DUPLICATEBUILDER_H
