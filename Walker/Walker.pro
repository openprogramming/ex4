#-------------------------------------------------
#
# Project created by QtCreator 2013-12-15T12:05:03
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Walker
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp \
    finder.cpp \
    duplicatebuilder.cpp

HEADERS += \
    finder.h \
    duplicatebuilder.h
